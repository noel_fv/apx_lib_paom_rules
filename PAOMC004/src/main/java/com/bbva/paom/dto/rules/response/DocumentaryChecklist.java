package com.bbva.paom.dto.rules.response;

import com.bbva.apx.dto.AbstractDTO;
import org.codehaus.jackson.annotate.JsonProperty;

public class DocumentaryChecklist extends AbstractDTO {

	private static final long serialVersionUID = 217398552603874043L;
	@JsonProperty("id")
	private String id;
	private Boolean isRequerid;
	private String status;
	private String condition;
	private String description;

	public DocumentaryChecklist() {
	}

	public DocumentaryChecklist(String id, Boolean isRequerid, String status, String condition, String description) {
		this.id = id;
		this.isRequerid = isRequerid;
		this.status = status;
		this.condition = condition;
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getRequerid() {
		return isRequerid;
	}

	public void setRequerid(Boolean requerid) {
		isRequerid = requerid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "{" +
				"id:'" + id + '\'' +
				", isRequerid:" + isRequerid +
				", status:'" + status + '\'' +
				", condition:'" + condition + '\'' +
				", description:'" + description + '\'' +
				'}';
	}
}
