
package com.bbva.paom.dto.rules.request;

import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;


public class ProcessInfo implements Serializable {


	private String processTypeCode;

	public ProcessInfo() {
	}

	public ProcessInfo(String processTypeCode) {
		this.processTypeCode = processTypeCode;
	}

	public String getProcessTypeCode() {
		return processTypeCode;
	}

	public void setProcessTypeCode(String processTypeCode) {
		this.processTypeCode = processTypeCode;
	}

	@Override
	public String toString() {
		return "processInfo{" +
				"processTypeCode:'" + processTypeCode + '\'' +
				'}';
	}
}
