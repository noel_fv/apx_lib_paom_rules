
package com.bbva.paom.dto.rules.request;

import org.codehaus.jackson.annotate.JsonProperty;
import java.io.Serializable;


public class Branch implements Serializable {

	@JsonProperty("id")
	private String id;

	public Branch() {
	}

	public Branch(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Branch{" +
				"id:'" + id + '\'' +
				'}';
	}
}
