
package com.bbva.paom.dto.rules.request;

import org.codehaus.jackson.annotate.JsonProperty;
import java.io.Serializable;


public class Channel implements Serializable {

	@JsonProperty("id")
	private String id;

	public Channel() {
	}

	public Channel(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Channel{" +
				"id:'" + id + '\'' +
				'}';
	}
}
