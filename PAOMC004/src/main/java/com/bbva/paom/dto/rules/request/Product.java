
package com.bbva.paom.dto.rules.request;

import org.codehaus.jackson.annotate.JsonProperty;
import java.io.Serializable;


public class Product implements Serializable {

	@JsonProperty("id")
	private String id;

	public Product() {
	}

	public Product(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
