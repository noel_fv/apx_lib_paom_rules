
package com.bbva.paom.dto.rules.request;

import org.codehaus.jackson.annotate.*;

import java.io.Serializable;
import java.util.Map;


public class RulesRequestBO implements Serializable {

	private Branch branch;
	private Channel channel;
	private Product product;
	@JsonIgnore
	private Map<String,Object> dataDinamyc;

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Map<String, Object> getDataDinamyc() {
		return dataDinamyc;
	}

	public void setDataDinamyc(Map<String, Object> dataDinamyc) {
		this.dataDinamyc = dataDinamyc;
	}

	@Override
	public String toString() {
		return "{" +
				"" + branch + '\'' +
				"," + channel +
				"," + product + '\'' +
				"," + dataDinamyc;
	}
}
