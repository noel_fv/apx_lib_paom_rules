
package com.bbva.paom.dto.rules.response;

import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;
import java.util.List;


public class RulesResponseBO implements Serializable {

	/**
	 * {"DocumentaryChecklist": [
	 *       {
	 * 		    "id": "0001"
	 * 		    "isRequerid": true
	 * 			"status": "IMPORTANTE"
	 * 		    "condition":"Incluir si la dirección del DNI no es la actual"
	 * 			"description": "no se"
	 *        }
	 *  ]}
	 */
	private static final long serialVersionUID = -4592599458754802093L;

	@JsonProperty("DocumentaryChecklist")
	private List<DocumentaryChecklist> documentaryChecklist;


	public List<DocumentaryChecklist> getDocumentaryChecklist() {
		return documentaryChecklist;
	}

	public void setDocumentaryChecklist(List<DocumentaryChecklist> documentaryChecklist) {
		this.documentaryChecklist = documentaryChecklist;
	}

	@Override
	public String toString() {
		return "[documentaryChecklist=" + documentaryChecklist + "]";
	}

}
