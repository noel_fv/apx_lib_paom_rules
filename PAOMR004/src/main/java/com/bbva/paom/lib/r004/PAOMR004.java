package com.bbva.paom.lib.r004;


import com.bbva.paom.dto.rules.response.RulesResponseBO;

public interface PAOMR004 {

	RulesResponseBO executeRulesInformation(String urlServicesRules, String jsonBodyRequest);

}
