package com.bbva.paom.lib.r004.impl;

import com.bbva.paom.dto.rules.response.RulesResponseBO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestClientException;

public class PAOMR004Impl extends PAOMR004Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger(PAOMR004Impl.class);
	private static final String TRACE_PAOMR004_IMPL = "[PAOM][PAOMR004_IMPL] - %s";


	/**
	 * Metodo de integracion al servicio de rules
	 * @param urlServicesRules Url del servicio a consultar
	 * @param jsonBodyRequest  Cuerpo del request a enviar
	 * @return RulesResponseBO Objeto que contiene la informacion de rules
	 */
	@Override
	public RulesResponseBO executeRulesInformation(String urlServicesRules, String jsonBodyRequest) {

		LOGGER.debug(String.format(TRACE_PAOMR004_IMPL, "START executeRulesInformation()"));
		RulesResponseBO response = new RulesResponseBO();
		try {

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			final HttpEntity<String> request = new HttpEntity<>(jsonBodyRequest, headers);
			LOGGER.info(String.format(TRACE_PAOMR004_IMPL, "JSON Input: " + request.toString()));
//			this.createConnectorBuilder();
			/*
			 * URL API CONNECTOR
			 * https://rules.work-02.ether.igrupobbva/v0/ns/{ns}/decisions/{decisions}/
			 * versions/{versions}/decision-services/DecisionService:evaluate
			 */
			response = this.internalApiConnector.postForObject(urlServicesRules, request, RulesResponseBO.class);
			LOGGER.info(String.format(TRACE_PAOMR004_IMPL, "JSON Output: " + response.toString()));

		} catch (RestClientException e) {
			LOGGER.info(String.format(TRACE_PAOMR004_IMPL, "Error executeRulesInformation() " + e));
			this.addAdvice("PAOM00000001");
		}
		return response;
	}

	//	private void createConnectorBuilder() {
//		LOGGER.info(String.format(TRACE_PCLDR005_IMPL, "createConnectorBuilder START"));
//
////		if (this.internalApiConnector == null) {
////			String certificateId = "CERTIFICADO ETHER"; // bot-apx-fraude.p12 chameleon.certificate.internal.id
//		String certificateId = this.applicationConfigurationService.getProperty("chameleon.certificate.internal.id"); // bot-apx-fraude.p12
//
//		LOGGER.info(String.format(TRACE_PCLDR005_IMPL, "certificateId" + certificateId));
//		LOGGER.info(String.format(TRACE_PCLDR005_IMPL, "Get Certificate of Chameleon Service"));
//		String certificateChameleonInternal = this.chameleonService.getCertificate(certificateId);
//
//		LOGGER.info(String.format(TRACE_PCLDR005_IMPL, "Get PWD of Chameleon Service"));
////			String certificatePasswordInternal = "PASSWORD ETHER";//TU PASSWORD  chameleon.certificate.internal.password"
//		String certificatePasswordInternal = this.applicationConfigurationService
//				.getProperty("chameleon.certificate.internal.password");
//		LOGGER.info(String.format(TRACE_PCLDR005_IMPL, "password" + certificatePasswordInternal));
//
//		this.internalApiConnector = this.apiConnectorBuilder.internal()
//				.withChameleon(certificateChameleonInternal, certificatePasswordInternal).build();
//
////		}
//		LOGGER.info(String.format(TRACE_PCLDR005_IMPL, "createConnectorBuilder END"));
//
//	}


}
