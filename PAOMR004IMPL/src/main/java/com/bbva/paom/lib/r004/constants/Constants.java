package com.bbva.paom.lib.r004.constants;

public interface Constants {

    String API_RULES = "rules";
    String JSON_START = "{\"comodin\":";
    String JSON_END = "}";
    String PARAM_NS = "ns";
    String PARAM_DECISIONS = "decisions";
    String PARAM_VERSIONS = "versions";
    String NAMESPACE_NAME = "NAMESPACE_NAME";
    String RULE_NAME = "RULE_NAME";
    String VERSION_NAME = "VERSION_NAME";
    String CONTRACTING_CODE = "contractingCode";
    String CHANNEL_CODE = "channelCode";
    String PROCESS = "process";
    String PRODUCT_ID = "productId";
    String PRODUCT = "product";
    String COMODIN = "comodin";
    
}
